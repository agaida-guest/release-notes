# Translation of Debian release notes to French
# Copyright (C) 2005-2009 Debian French l10n team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the Debian release notes.
#
# Translators:
# Denis Barbier, -2004.
# Frédéric Bothamy <frederic.bothamy@free.fr>, 2004-2007.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2008-2009.
# David Prévot <david@tilapin.org>, 2013.
# Christian Perrier <bubulle@debian.org>, 2013.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2014, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2019-05-14 11:27+0200\n"
"PO-Revision-Date: 2017-06-13 14:00+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "fr"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Plus d'informations sur &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Lectures pour aller plus loin"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Outre ces notes de publication et le manuel d'installation, d'autres "
"documents sont disponibles sur le projet de documentation Debian (DDP) dont "
"le but est de créer une documentation de qualité pour les utilisateurs et "
"les développeurs Debian. On peut y trouver des documents comme la référence "
"Debian, le guide du nouveau responsable Debian, la foire aux questions (FAQ) "
"Debian et d'autres encore. Pour tous les détails concernant les ressources "
"disponibles, veuillez consulter le <ulink url=\"&url-ddp;\">site web de la "
"Documentation Debian</ulink> et le <ulink url=\"&url-wiki;\">wiki Debian</"
"ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"La documentation de chaque paquet est installée dans <filename>/usr/share/"
"doc/<replaceable>paquet</replaceable></filename>. Elle peut contenir les "
"informations concernant le copyright, les détails spécifiques à Debian et "
"toute la documentation d'origine."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Obtenir de l'aide"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Il y a beaucoup de sources d'aide et de conseils possibles pour les "
"utilisateurs de Debian, cependant, on ne devrait les utiliser qu'après avoir "
"fait des recherches sur le problème dans la documentation. Cette section "
"fournit une courte introduction aux sources qui peuvent être utiles aux "
"nouveaux utilisateurs de Debian."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Listes de diffusion"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Les listes de diffusion les plus intéressantes pour les utilisateurs Debian "
"sont les listes debian-user (en anglais), debian-user-french (en français) "
"et les autres listes debian-user-<replaceable>langue</replaceable> (pour les "
"autres langues). Pour plus d'informations sur ces listes et des précisions "
"sur la façon de s'y inscrire, lisez <ulink url=\"&url-debian-list-archives;"
"\"></ulink>. Veuillez chercher la réponse à votre question dans les archives "
"avant de poster sur la liste et veuillez suivre la charte de ces listes."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "Chat (IRC)"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian a un canal IRC (pour les anglophones) dédié à l'aide et à "
"l'assistance aux utilisateurs Debian. Il est situé sur le réseau IRC OFTC. "
"Pour accéder au canal, pointez votre logiciel client IRC favori sur irc."
"debian.org et rejoignez le canal <literal>#debian</literal>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Merci de suivre les usages du canal de discussion et de respecter pleinement "
"les autres utilisateurs. Les règles d'utilisation sont disponibles dans le "
"<ulink url=\"&url-wiki;DebianIRC\">Wiki Debian</ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Pour plus d'informations sur OFTC, veuillez visiter le <ulink url=\"&url-irc-"
"host;\">site web</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Signaler les bogues"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Nous nous efforçons en permanence de faire de Debian un système "
"d'exploitation de qualité ; cependant cela ne signifie pas que les paquets "
"que nous fournissons sont totalement exempts de bogues. En accord avec la "
"philosophie de <quote>développement ouvert</quote> de Debian, nous "
"fournissons à nos utilisateurs toutes les informations sur les bogues qui "
"nous ont été signalés sur notre système de suivi des bogues (BTS). Le BTS "
"est consultable à l'adresse <ulink url=\"&url-bts;\"></ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Si vous trouvez un bogue dans la distribution ou dans un logiciel qui en "
"fait partie, merci de le signaler afin que nous puissions le corriger pour "
"les prochaines versions. Signaler les bogues nécessite une adresse "
"électronique valide. Nous demandons cela afin que les développeurs puissent "
"entrer en contact avec les personnes qui ont envoyé le rapport de bogue au "
"cas où plus d'informations seraient nécessaires."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Vous pouvez soumettre un rapport de bogue en utilisant le programme "
"<command>reportbug</command> ou en envoyant un courrier électronique. Vous "
"trouverez plus d'informations sur le système de suivi des bogues (BTS) et "
"les moyens de l'utiliser dans la documentation de référence (disponible à "
"<filename>/usr/share/doc/debian</filename> si vous avez installé <systemitem "
"role=\"package\">doc-debian</systemitem>) ou en ligne sur le site du <ulink "
"url=\"&url-bts;\">système de suivi des bogues</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Contribuer à Debian"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Il n'est pas nécessaire d'être un expert pour contribuer à Debian. En aidant "
"les utilisateurs qui ont des problèmes sur les diverses <ulink url=\"&url-"
"debian-list-archives;\">listes</ulink> d'assistance vous contribuez à la "
"communauté. Identifier (et également résoudre) les problèmes liés au "
"développement de la distribution en participant aux <ulink url=\"&url-debian-"
"list-archives;\">listes</ulink> de développement est aussi très utile. Pour "
"maintenir la grande qualité de la distribution Debian, <ulink url=\"&url-bts;"
"\">signalez les bogues</ulink> et aidez les développeurs à les trouver et à "
"les résoudre. L'application <systemitem role=\"package\">how-can-i-help</"
"systemitem> vous aide à trouver des bogues signalés adaptés sur lesquels "
"vous pouvez travailler. Si vous êtes plutôt un littéraire, vous voudrez peut-"
"être contribuer plus activement en écrivant des <ulink url=\"&url-ddp-vcs-"
"info;\">documentations</ulink> ou en <ulink url=\"&url-debian-i18n;\"> "
"traduisant</ulink> la documentation existante dans votre langue."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Si vous pouvez consacrer plus de temps, peut-être pouvez-vous gérer, au sein "
"de Debian, un des logiciels de la grande collection des logiciels libres. Il "
"est très important d'adopter ou de maintenir les programmes qui font partie "
"de Debian. La <ulink url=\"&url-wnpp;\">base de données sur le travail à "
"faire et les futurs paquets</ulink> détaille ces informations. Si vous êtes "
"intéressé par des groupes particuliers, alors il vous plaira peut-être de "
"contribuer à certains <ulink url=\"&url-debian-projects;\">sous-projets</"
"ulink> de Debian, comme les portages vers des architectures particulières et "
"les « <ulink url=\"&url-debian-blends;\">Debian Pure Blends</ulink> » pour "
"les groupes d’utilisateurs particuliers, parmi bien d’autres."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"Quelle que soit la manière dont vous travaillez dans la communauté du "
"logiciel libre, en tant qu'utilisateur, programmeur, rédacteur ou "
"traducteur, vous aidez la communauté. Contribuer est gratifiant, amusant, "
"et, en même temps, cela vous permet de rencontrer de nouvelles personnes et "
"cela vous vous donne chaud au c&oelig;ur."
